﻿using UnityEngine;
using FYFY;
using FYFY_plugins.PointerManager;

public class ManualMovingSystem : FSystem {

    private Family _movableGO = FamilyManager.getFamily(
        new AllOfComponents(typeof(PointerOver)));

    protected override void onProcess(int familiesUpdateCount)
    {
        foreach (GameObject go in _movableGO)
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 oldPos = go.transform.position;
                Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                newPos.z = 0;
                go.transform.position = newPos;
                go.GetComponent<Rigidbody2D>().AddForce((newPos - oldPos) * 5);
            }
        }
    }
}