﻿using UnityEngine;
using System.Collections;
using FYFY;

public class DivisionSystem : FSystem
{

    private Family _divideGO = FamilyManager.getFamily(
        new AllOfComponents(typeof(Division)));

    // Use to process your families.
    private void onGOEnter(GameObject go)
    {
        GameObject prefab = go.GetComponent<Division>().prefab;
        GameObject newGO = GameObject.Instantiate(prefab, go.transform);
    }

    protected override void onProcess(int familiesUpdateCount)
    {
        foreach (GameObject go in _divideGO)
        {
            GameObject prefab = go.GetComponent<Division>().prefab;
            GameObject newGO = GameObject.Instantiate(prefab, go.transform);
            newGO.transform.Translate(new Vector2(0, -2));
            GameObjectManager.unbind(go);

            foreach(Transform child in go.transform)
            {
                child.localScale /= 2;
            }
        }
    }
}