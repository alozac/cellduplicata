using UnityEngine;
using FYFY;
using FYFY_plugins.CollisionManager;

public class RandomMovingSystem : FSystem {
    
    private Family _randomMovingGO = FamilyManager.getFamily(
        new AllOfComponents(typeof(Move), typeof(RandomTarget)));

    private Family _inCollisionGO = FamilyManager.getFamily(
        new AllOfComponents(typeof(InCollision2D)));

    public RandomMovingSystem(){
        foreach (GameObject go in _randomMovingGO){
            onGOEnter(go);
        } 
        _randomMovingGO.addEntryCallback(onGOEnter);
    }
    
    private void onGOEnter(GameObject go){
        // Transform tr = go.GetComponent<Transform>();
        // RandomTarget rt = go.GetComponent<RandomTarget>();
        // rt.target = tr.position;

        Move mv = go.GetComponent<Move>();
        
        RandomTarget rt = go.GetComponent<RandomTarget>();
        rt.target = Random.insideUnitCircle;
        Rigidbody2D rigidbody2D = go.GetComponent<Rigidbody2D>();
      //  rigidbody2D.velocity = rt.target * mv.speed;
    }

	// Use to process your families.
	protected override void onProcess(int familiesUpdateCount) {
	    foreach(GameObject go in _inCollisionGO){
            Rigidbody2D rigidbody2D = go.GetComponent<Rigidbody2D>();
            RandomTarget rt = go.GetComponent<RandomTarget>();

            Move mv = go.GetComponent<Move>();
          //  rigidbody2D.velocity = -rigidbody2D.velocity;
        }
	}
}