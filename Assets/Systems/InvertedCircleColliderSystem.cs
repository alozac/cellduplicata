﻿using UnityEngine;
using FYFY;
using FYFY_plugins.CollisionManager;

public class InvertedCircleColliderSystem : FSystem {

    private Family _hasInvertedCircleEdgeGO = FamilyManager.getFamily(
        new AllOfComponents(typeof(InvertedCircle)));

    public InvertedCircleColliderSystem()
    {
        foreach (GameObject go in _hasInvertedCircleEdgeGO)
        {
            InvertedCircle invertedCircle = go.GetComponent<InvertedCircle>();
            int numEdges = invertedCircle.numEdges;
            Vector2[] points = new Vector2[numEdges];

            for (int i = 0; i < numEdges; i++)
            {
                float angle = 2 * Mathf.PI * i / numEdges;
                float x = 2*Mathf.Cos(angle);
                float y = 2*Mathf.Sin(angle);

                points[i] = new Vector2(x, y);
            }
            GameObjectManager.addComponent<Rigidbody2D>(go, new { bodyType = RigidbodyType2D.Static });
            GameObjectManager.addComponent<CollisionSensitive2D>(go);
            GameObjectManager.addComponent<EdgeCollider2D>(go, new { points = points });
        }
    }
}
