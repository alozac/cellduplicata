using UnityEngine;

public class RandomTarget : MonoBehaviour {
	// Advice: FYFY component aims to contain only public members (according to Entity-Component-System paradigm).
	[HideInInspector] //rends impossible � modifier
	public Vector3 target;
}